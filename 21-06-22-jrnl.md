# June 21 Code Journal

After the web and apps meeting today, I investigated FastAPI a bunch, set up a repo to connect it to nuxt3, and did some brainstorming on what the data organization of this all might look like.

## Useful Tutorials
- [FastAPI Youtube Tutorial](https://www.youtube.com/watch?v=-ykeT6kk4bk)
- [FastAPI + Vue](https://testdriven.io/blog/developing-a-single-page-app-with-fastapi-and-vuejs/)

## FastAPI, Nuxt3, Strapi Configuration
- Why not just Vue?
	- _because nuxt handles a lot of configuration out of the box, plus I'm already setting up an example using vanilla vue_

- [Project Repo](https://gitlab.com/lilyx13/fastapi-nuxt-strapi-demo)
	- This is also doubling as a place to start planning out how to organize components for our websites (which I began doing this afternoon)

### What I learned
- FastAPI is much lighter than Flask, because it does not have a presentation layer built in, it is much better suited to our needs.
- We should be really clear about where we are storing data and how all the access points will work.
	- For instance, Strapi is a great solution for us because it allows not coders to be able to edit information. It also functions as an sql database. However it is not meant for processing, manipulating, or doing anything with the data. Just sending it places.
	- FastAPI serves as a great backend for managing, manipulating, and sending data. While some data should go straight from the CMS to a page (ie: Flock), other data might be better managed here.
	- Nuxt also has it's own server built in. We will probably not use this too much, though we could actually do all of this without FastAPI. Therefore it may be better to stick to vanilla vue for a more lightweight setup. However this will require a lot more work to configure for SSG etc. Nuxt manages that for us and offers a really easy approach to routing.

## Data Organization

Page specific content can be stored with each page, stuff that a frontend editor would need to be able to tweak and change quickly and easily. However I also think it would be all in all better for there to be a single way that data is stored and organized... or at least not introduce more ways than necessary.

I'm pretty early in my notes. But here are some of the topics that I've been assessing
- Release Information (updates multiple places, date relevant, time sensitive)
- Media Content (fedora wide, page level, blog post, event...)
- Translations (this is a huge deal for Fedora, I mostly just reviewed how it's currently being handled).
	- translation scripts are all in the backend at present. This would be a good way to do things because things can be translated once and then sent out as opposed to translating on the frontend. However an issue with this is that **all** of the text content would have to come from the backend. 
	This requires more research and consultation
