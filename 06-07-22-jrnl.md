# July 6 Journal

I wasn't able to complete a few journal entries last week due to a series of migraines.

Some quick updates on work that I've done in the past week

- UX research on Fedora web navigation and sitemap
- Set up a frontend vue 3 workspace
- Networked with Vue School for particpation in a hackathon that they are hosting next week (to build Fedora Web & Apps dev team Vue skills)
- Planned and proposed a Nest Dev presentation
- Planned and proposed a Nest UX presentation
- Opened a MR for a fix in Fedora bootstrap
- Removed banners for community survey
- Red Hat intern Volunteer Day
