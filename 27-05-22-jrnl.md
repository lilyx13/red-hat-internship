# May 27 2022 Code Journal

- Container Workshop with Research team
  - notes taken in hackmd, mostly theoretical
  - Refer to the SRE training doc in google docs for training links and extra documentation
- Fedora UI Components

## Fedora UI Components
- Fixed bugs from yeterday's brute force approach
- Footer is now behaving exactly as desired
  - except the + sign. that needs to rotate on click to a x
    - TODO figure out how to do that in bootstrap
- Header
  - Stepped up from bootstrap
  - created reusable components for dropdowns, links, and brand link
  - A need to start using Fedora Bootstrap is coming soon. I can get away with a bit more in vanilla, and I know that once I start using Fed boot, that will start taking up time. So i'm going to do as much as I can in vanilla before I migrate.

## Design Review
- Wrote a review for the IoT page design