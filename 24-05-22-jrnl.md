# May 24 2022 Code Journal

- Fedora Website Footer

## Fedora Website Footer
- Today I improved the sponsor section and fedora logo + docs links section. Their mobile layout was not working but now I think it's looking good
- I also cleaned up the column layout and improved it's responsive behaviour
- **Big Win** I spent too much time trying to get the bootstrap dropdown to work in codepen. however I think it was because of the parent-child relationship between the element that I put the toggle on `<h4><a></a></h4>`.
  - I've fixed this and am now able to get the links to fold into dropdowns.
    - **however** they aren't yet changing between layouts from desktop to mobile. This tag is also better set up as a `<nav>` which I finished today by starting.
    - My next step will be to try using the bootstrap nav responsive pattern. It may be as simple as switching from the top level element.
    - If this doesn't work, I think that the layout might need to incorporate a `v-if` for better templating... but this will probably be more costly so I want to avoid this
- [Navigate here to see components](https://gitlab.com/lilyx13/fedora-vue-web-components)
## Other Notes
- I learned more organizational aspects about Red Hat and how to navigate within it. 
- I discussed the Flock design with the Web and Apps team. we floated the idea of using a headless cms and potentially pulling data from the hopin api for virtual events.
  - headless cms ideas: [Strapi](https://strapi.io/), [Payload](https://payloadcms.com/)

## Tomorrow Plan Notes
- Add details to issue board:
  - estimated dates
  - task descriptions
  - priorities
- **Work on Flock Design** -- Priority
  - General of desktop layout
- Finish footer nav responsive design
- Meet with the Design team
  - review my Flock design
    - Feedback on feature hierarchy
- Set up my daily task planner and fill out all tasks for this week
