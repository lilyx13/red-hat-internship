# June 8 2022 Code Journal

- Optimizing Design and Implementing Modifications

## Design Optimization
Today was primarily spent on implementing changes that I discussed with Mo as well as the Web and Apps team yesterday. I also unfortunately made my original on a really small artboard which isn't great for really assessing space and element size. So I'm doing my final version on a new page that's using an 1920w artboard.

My error here was kind of emotionally aggrivating. I had been really ready to be done with this stage of the design but it seems that I am not. My goal is to be done next Tuesday, where I'll pass it on for community review and then make final tweaks based on that and a stakeholder review.

A part of why this is now taking so much longer is because I'm making a point to use standardized elements, testing them as well, including the margins etc on my artboard. With any luck this can be the basis for a set of reusable elements that will mean less design time later, and more consistent designs. So while it's great that I'm able to test sizes n stuff effectively, it's becoming a bit of a drain on my energy level.

### Changes
Primary changes that I made today were to the hero section and to the past content section. I tested another 3 column card layout for important dates but think that I'll keep that vertical. I also want to put the fedora characters in the white space of that part.

Tomorrow will be a bit of a big change day. With a focus on adjustments to the hybrid event depiction as well as the event calendar. That's where I think the wind went out of my sails because those two components were particularly challenging to come up with so I'm not sure how many spoons I have at this time to adjust those. I think tomorrow I want to avoid designing all day and will instead switch to do some training on podman and test setting up Flask the way I would Vue with the component architecture. 

Then on Friday I can be sure to finish this design and be able to move to coding on monday.
