# July 8 2022 Code Journal

Today was very productive for getting the vue code project moving forward.

- navigation
- components
- workstation page
- bootstrap
- presentations

---

## Navigation
I focused on two steps of fedora's navigation

### Step 1: Vue Router
- added a bunch of pages to the router (no children yet, which will probs need to be changed as we go forward)
	- we may also benefit from splitting the router arrays up into multiple files for easier labelling and organizing. At this point I just have a few set up for getting moving so it's not a huge deal
- Set up fallback routes for pages that aren't in the vue repo yet
	- this means we can use vue router and not have to change too much as we continue with our migration
- **See [the router/index file](https://gitlab.com/lilyx13/fedora-web-fav-stack/-/blob/main/client/src/router/index.ts)**
	- note commented out `beforeEnter(){}` sections
	- **TODO** test to make sure that this method does not cause any vulnerabilities or performance delays
- lazy loaded pages use `component: () => import()` syntax so they are only loaded when requested
- non lazy loaded pages are loaded

### Step 2: Component for older pages
- I copied the vue router `routes` array and made slight modifications to test creating a component that can be dropped into all of the pages that aren't migrated yet
- The goal of this is that we will be able to create a unified navigation system across all of fedora's websites and apps and migrate pages one by one without having any serious disruptions to services or workflow
- This is not production ready, but does show that it should work alright
- TODOS:
	- move component into external js file and drop that into the test page
	- find a way to edit the links so that they won't have to be changed on each old page when we need to make an edit.
		- **GOAL: When we need to change a url because of migration, we should only have to change it in 1 or 2 places (2 being 1 for the vue router and 1 that feeds everything else)**
- [repository](https://gitlab.com/lilyx13/vanilla-site-add-vue)

## Components
- Today I took a macro approach to our components. Starting by drafting out the content needed on the **workstation** page
	- My focus at this stage is to determine the most productive and consistent workflow for component usage
		- workflow 1: Simplest components:
			- dynamic content passed as a prop
			- static text is slotted
			- **would feel like using an `<a></a>` tag**
		- workflow 2: Looped components such as lists:
			- pass an array of information to the component
			- component generates multiple child elements based on the arrays and objects passed into it
			- slots used for content that doesn't quite fit the majority use cases
			- **takes advantage of `v-for` syntax and ability to pass lists of info, reduced html editing later**
		- workflow 3: Complex components (cards, heros)
			- Named slots are used for sections (I think if there's 3+ rows in a component, this is a good time for this configuration
			- Background content is passed as a prop
			- Component is utilitiy focused, focusing on how the elements are aligned at different screen sizes
			- Named slots allow for a diverse amount of layout changes
				- ie: in the hero sections, we currently have 3 ways that buttons are positioned, slotted styles can give us default options with each named slot to account for variation while still maintaining standardized ways of doing things
- So while I did not fill out much in the components, I just tried dropping them onto a page and took note of how I felt each one would work best (resulting in the above 3 workflow strategies)
- TODO: Copy this note into the doc in the project repo

## Workstation Page
- I drafted out all the sections for the workstation page and imported the components that I think should be used
- This was a top level focus to plan out a good strategy for generating components, in the past I've built components in a way that felt good while creating them, and then required a bunch of workarounds because the usage workflow was not very smooth
- I followed the mockup in penpot to do this

## Bootstrap 5
- I made a merge request two days ago to fix a build command descrepancy in the master repo
- Today I went to make another merge request however since it's to the same branch and that I have one open already, I couldn't so I just made an issue
	- this was for typography, the fedora-bootstrap doesn't have montserrat fonts in it, so I added them in my fork
	- there also aren't files for the "Hack" typography listed, so I added those to mine, but also opened an issue because it makes more sense to use "Source Code Pro" asit's the default on fedora. I don't care either way but the files need to be set up properly
- We have another issue for making this integrate smoothly with vue or react or any other node based application or framework
	- fedora-bootstrap cannot be installed via npm. we have to copy files over, which creates an extra step for everyone's workflow
	- if we could make it so that updates can be made by updating the npm package, imo this will create a much smoother workflow and will really help us as we move forward with different stacks (especially node stacks)
		- I created an issue in the master repo asking about the potential for registering fedora-bootstrap with NPM
- Otherwise, I was not able use fedora-bootstrap in my work, so I installed regular bootstrap5 for the time being, but this is a temporary solution
	- **Note: it might be that I just don't understand quite properly how to add the files to vue, however in my goal of trying to stick to the most common workflows, I already feel like I'm making a workaround where there is a well established convention that we should follow**

---

## Presentations
- Intern Midpoint presentations:
	- google slides presentation using a Red Hat template to showcase my current work and outline what my next steps are:
		- work done:
			- UI Design for Flock Page
			- UX Research for a unified navigation for Fedora
			- Stack Research and experimenting to determine a powerful, but accessible stack for our team to use in our website revamp
		- next steps:
			- Create a component system that is consistent and logically provides solutions to frontend design objectives
			- Build the Fedora Workstation page and it's sub pages
			- Create a reusable component that can be dropped into the older pages so that we can take advantage of a new nav setup and evolve it while we migrate our web pages
- Vue Forge:
	- Hackathon on July 13-14 with Onuralp
	- Goal is to create a trello clone
	- Network with the vue community
	- Learn more about Vue and hopefully gain knowlege that will help us avoid problems as we migrate
- Nest Dev Session:
	- Demonstrate building fedora websites with Vue
	- Somewhat Instructional Session to establish a pipeline for new contributors to learn and build their skills
- Nest UX Session:
	- Discuss UX research and development process in context of making a navigation system for fedora's websites
	- Show collaboration between Dev and Design
	- Get participants for a small UX study to gain feedback on new navigation system
