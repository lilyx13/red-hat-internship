# May 26 2022 Code Journal

- Fedora Contributor Survey Banner
- New Fedora Footer Component
- New Fedora Header Component
- Meeting

## Contributor survey banner
- Created a collapsable background with a gradient that mostly matches the gradient in the image. This allows a full width banner that doesn't get too big vertically because the image is capped at 800px max width
- Added it also to the start page and have messages etc ready for comblog etc
- **waiting** on the actual survey link. once I get that, I can make all this go live

## Fedora Footer Element
- [Repo](https://gitlab.com/lilyx13/fedora-vue-web-components)
- Finished layout and responsive design structure
- Dropdown links at mobile are working
- Need to create sub components for code to not be rediculous
- 

## Header Component
- Set up boiler plate for the header, I analyzed this code to figure out how to make better looking dropdowns in the footer. the actual dropdown classes looked gross but the nav behaviour worked well.
- **Note** All the links in the footer and header are the same. I want to make this work so that they only need to be changed once. 
  - create a separate .js file that contains all the arrays and import them into the header and footer components