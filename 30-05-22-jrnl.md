# May 30 2022

- Site architecture experimentation
- Design Planning
- Strapi

## Strapi
- I easily set up and configured a strapi cms with some sample content to demonstrate tomorrow at the web and apps meeting using Postman to show how the API is reached

## Design Planning and work
- I got the idea to do a sort of collage design for the Flock hero using images from past Flock events.
  - the reason for this is that there aren't really any good catching images that I can use that won't be either bad resolution and unprofessional looking or easily dated. So By having a group of smaller images with a gradient overlay, we can easily update it and be less dependent on 1 good shot. I pulled in some images from my collection that I was gathering earlier and started planing how to lay them out
- Started creating the accordion card redesign for flock

## Optimized navigation setup
- Because there are so many links that will need to be in the navbar and hero footer, I think it'll be more optimized to have the links stored in a single .js file in assets/ and have them feed both navigations. That way if there are changes or updates, they only need to be made in 1 place

## Site Architecture Experimentation
- This was a challenging part of the day. I tried setting up a few different SSG/SSRs. I admit that I hit some limits in my knowledge on this subject
- Things that I tried out and their results:
  - Flask (Server) + Vue (Client using Vite)
    - Probably the most community friendly setup. But the one that hit the limit of my knowledge fastest
    - I couldn't quite get how to render the Vue pages on the client as my memory of how to use Flask is somewhat dated
  - Vite-SSG Vue
    - This was the most simple looking concept and would be the fastest, if I could get it to work
      - my issue was that I couldn't render files from `views/`, so I used `vite-plugin-pages` to render static pages, however I misunderstood the docs on how to get Vue Router to point to the pages folder instead.
      - I tried a few different configurations and wasn't able to get it to work and the documentation was lacking, not boding well for this project
  - Vite-Plugin-SSR
    - This actually worked really easily. It's file structure is a bit different from what I'm used to with a vue app (at least the template) but it was able to bet set up with a single command and the build commands were one shot deals
    - This is kind of like a leaner version of nuxt
  - Nuxt
    - This is where I'm most comfortable but I'm not sure if we want to commit to a static site generator, or move that far into JS territory. Personally I think this gives us the best bang for our buck, but I'm not sure if the team will go for it.
    - Downsides are that we will be more dependent on their generator... but then if we were to use literally any other static site generator (like Flask for instance), we are still dependant on a bunch of dependencies and constraints there as well.
  - Vue SSR Rendering
    - I looked a bit at the docs on this at the end of the day. I believe that this is how we would successfully create a client Vue app to render from Flask and Frozen Flask. However I was not able to do it myself and think that I could benefit from some help.