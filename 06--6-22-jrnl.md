# June 6 2022

## Flock Design
- Today was very productive, I finished a UI design of how we can add events for Flock etc directly on the page to make it more interactive, including add to calendar etc
- I also designed a Sponsorship section, pulling some information from the prospectus and putting it on the home page. For this I used a UI pattern for lists that's been applied to Workstation and IoT
	- I got rid of the tierd presentation of the sponsors on the web page. I'm not sure if it's useful there, I'm seeing a lot of sites not showing the tiers at that spot, plus it's not addressed anywhere in the prospectus. Instead I'm going for a sorta asymmetrical grid that echos the pattern used in the Format section. Logos will be organized by size instead of top down. This will allow for a side scroll banner at mobile which will save us a lot of space.
- The only step left is to put actual pictures in the gallery grids that I made last week. This should pull the hero together through the page better. I'd like to have at least 1 more high resolution and dark colored banner image later down the page. maybe around the sponsors section. There's a conference photo that I found that looks pretty cool and might be a good candidate.
- I'd also like to switch the community image out for the 2018 image found on the prospectus. Otherwise we are going to have like 3 pages all using the same community image.
- There are some issues with my current design that need resolution:
	- The event calendar looks funny when I add hatch to it. but it would be important for that to be fully featured
	- The sponsor grid isn't great yet
	- The overall design feels dusty
